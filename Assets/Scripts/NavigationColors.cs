﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
    public static class NavigationColors
    {
        public static Color StartColor = Color.green;
        public static Color WaypointColor = Color.blue;
        public static Color FinishColor = Color.red;
        public static Color SelectedColor = Color.yellow;
        public static Color PathColor = Color.grey;
        public static Color WaypointRotationColor = Color.magenta;
        public static float WaypointsSize = 0.5f;
        public static float RotationLength = 2f;
    }
}
