﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
    public class Path : MonoBehaviour
    {
        
        //[SerializeField] private float GizmosSize = 0.5f;

        private List<Waypoint> Waypoints;

        private void Awake()
        {
            RefreshWaypoints();
        }

        private void OnDrawGizmos()
        {
            RefreshWaypoints();
            int SelectedWaypointIndex = -1;
            Waypoint SelectedWaypoint = FindSelectedWaypoint();
            if (SelectedWaypoint != null && SelectedWaypoint.transform.IsChildOf(this.transform))
            {
                SelectedWaypointIndex = SelectedWaypoint.Order;
            }

            for (int i = 0; i < Waypoints.Count; i++)
            {
                Gizmos.color = NavigationColors.WaypointColor;
                if (i == SelectedWaypointIndex)
                {
                    Gizmos.color = NavigationColors.SelectedColor;
                }
                else if (i == 0)
                {
                    Gizmos.color = NavigationColors.StartColor;
                }
                else if (i == Waypoints.Count -1)
                {
                    Gizmos.color = NavigationColors.FinishColor;
                }
                Gizmos.DrawSphere(Waypoints[i].transform.position, NavigationColors.WaypointsSize);
                
            }

        }

        private void RefreshWaypoints()
        {
            Waypoints = new List<Waypoint>();
            Waypoints.AddRange(GetComponentsInChildren<Waypoint>());
            Waypoints.Sort((x, y) => x.Order - y.Order);
            for (int i = 0; i < Waypoints.Count; i++)
            {
                Waypoints[i].name = "Waypoint (" + (i + 1) + ")";
            }
        }

        private Waypoint FindSelectedWaypoint()
        {
            GameObject SelectedGameObject = UnityEditor.Selection.activeGameObject;

            if (SelectedGameObject != null)
            {
                Waypoint SelectedWaypoint = SelectedGameObject.GetComponent<Waypoint>();
                if (SelectedWaypoint != null)
                {
                    return SelectedWaypoint;
                }
            }

            return null;
        }

        public void AddWaypoint()
        {
            GameObject newWaypoint = new GameObject();
            newWaypoint.transform.parent = this.transform;
            newWaypoint.transform.SetAsLastSibling();
            newWaypoint.transform.position = Vector3.zero;
            newWaypoint.transform.rotation = Quaternion.identity;
            Waypoint controller = newWaypoint.AddComponent<Waypoint>();
            if (Waypoints[Waypoints.Count -1].NextWaypoint == null)
            {
                Waypoints[Waypoints.Count - 1].NextWaypoint = controller;
            }
            UnityEditor.Selection.activeGameObject = newWaypoint;

        }

    }
}
