﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
    public class Waypoint : MonoBehaviour
    {
        
        public int Order
        {
            get
            {
                return transform.GetSiblingIndex();
            }
        }

        [Tooltip("How to move to next waypoint?")]
        public MovementType MovementType;

        [Tooltip("Should the NPC move to next waypoint automatically, or should it wait.")]
        public bool AdvanceToNextWaypoint;

        public Waypoint NextWaypoint;

        [Tooltip("Min/Max Delay in seconds between Animations")]
        public Vector2 Delay;

        public float GetDelay()
        {
            return Random.Range(Delay.x,Delay.y);
        }

        [Tooltip("Animation which will be played when an NPC reaches this waypoint, eg. 1 will play Aniamtion called \"Action1\".\n0 will play \"Idle\"")]
        public int ActionAnimation;

        public string GetAnimationName()
        {
            return "Action" + ActionAnimation;
        }

        [Tooltip("How many times should the Animation be repeated, set to 0 for inifinity.")]
        public int ActionLoops;

        private void OnDrawGizmos()
        {
            if (transform.parent == null || transform.parent.GetComponent<Path>() == null)
            {
                Gizmos.color = NavigationColors.WaypointColor;
                Gizmos.DrawSphere(transform.position, NavigationColors.WaypointsSize);
            }
            

            if (NextWaypoint != null)
            {
                Gizmos.color = NavigationColors.PathColor;
                Gizmos.DrawLine(transform.position, NextWaypoint.transform.position);
            }

            Gizmos.color = NavigationColors.WaypointRotationColor;
            Gizmos.DrawRay(transform.position, transform.forward * NavigationColors.RotationLength);
        }
    }

}