﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Navigation
{
    public enum MovementType
    {
        Run,
        Walk,
        Sprint,
        Teleport,
        None
    }
}
