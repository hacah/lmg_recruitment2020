﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation {
    [CreateAssetMenu(fileName = "Movement Speed Settings", menuName = "Navigation/NPC/Movement Speed Settings")]
    public class MovementSpeedSettings : ScriptableObject
    {
        public float WalkSpeed;
        public float RunSpeed;
        public float SprintSpeed;
        public float GetSpeed(MovementType movement) //Not using a Dictionary since Unity cannot serialize it without Odin
        {
            switch (movement)
            {
                case MovementType.Run:
                    return RunSpeed;
                case MovementType.Walk:
                    return WalkSpeed;
                case MovementType.Sprint:
                    return SprintSpeed;
                case MovementType.Teleport:
                    return float.MaxValue;
                default:
                    Debug.LogError("Magic!", this);
                    return 0f;
            }
        }
    }
}
