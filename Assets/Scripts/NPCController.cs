﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Navigation;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    [SerializeField] private Animator Anim;
    [SerializeField] private NavMeshAgent Agent;
    [SerializeField] private Waypoint CurrentWaypoint;
    private MovementType MovementMethod;
    [SerializeField] private MovementSpeedSettings MovementSpeed;
    [SerializeField]
    [Tooltip("Distance below which we consider a waypoint as reached.")]
    private float ReachDistance;
    private Coroutine Arrival = null;
    private float rotationSpeedAtDestination = 2f;
    private float rotationPrecisionAtDestination = 10f;

    private bool Wait = false;
    private int AnimationLoops = 0;

    public void SetStartingPoint(Waypoint waypoint)
    {
        //Debug.Log("Setting Starting Point");
        CurrentWaypoint = waypoint;
        MovementMethod = waypoint.MovementType;
        Arrival = StartCoroutine(ArriveAtDestination());
    }

    private void Awake()
    {
        if (Agent == null)
        {
            Agent = GetComponent<NavMeshAgent>();
        }
    }

    private void Start()
    {
        Agent.stoppingDistance = ReachDistance;
    }

    private void Move()
    {
        NavMeshPath navPath = new NavMeshPath();
        Agent.CalculatePath(CurrentWaypoint.transform.position, navPath);
        Agent.SetPath(navPath);
        if (MovementMethod != MovementType.Teleport)
        {
            Agent.speed = MovementSpeed.GetSpeed(MovementMethod);
            EnableSingleAnimation(MovementMethod);
        }
        else
        {
            Agent.speed = 0f;
            EnableSingleAnimation(MovementType.None);
            transform.position = CurrentWaypoint.transform.position;
        } 
        StartCoroutine(WaitUntillDestinationReached());
    }

    private IEnumerator WaitUntillDestinationReached()
    {
        yield return new WaitWhile(() => Agent.remainingDistance > ReachDistance);
        EnableSingleAnimation(MovementType.None);
        while (Quaternion.Angle(transform.rotation, CurrentWaypoint.transform.rotation) > rotationPrecisionAtDestination)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, CurrentWaypoint.transform.rotation, rotationSpeedAtDestination * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        Arrival = StartCoroutine(ArriveAtDestination());
    }

    public void GoToNextPoint(Waypoint waypoint = null)
    {
        ForceStopArrivalCoroutine();
        
        if (waypoint == null)
        {
            if (CurrentWaypoint.NextWaypoint == null)
            {
                return;
            }
            else
            {
                waypoint = CurrentWaypoint.NextWaypoint;
            }        
        }

        CurrentWaypoint = waypoint;
        MovementMethod = waypoint.MovementType;
        Move();
    }

    private IEnumerator ArriveAtDestination()
    {
        AnimationLoops = CurrentWaypoint.ActionLoops;
        bool InfiniteLoop = AnimationLoops == 0;

        while (AnimationLoops > 0 || InfiniteLoop)
        {
            float delay = CurrentWaypoint.GetDelay();
            yield return new WaitForSeconds(delay);
            if (CurrentWaypoint.ActionAnimation != 0)
            {
                Anim.Play(CurrentWaypoint.GetAnimationName(), 0);
                yield return new WaitForEndOfFrame();
                yield return new WaitUntil(() => IsIdle());
            }
            else
            {
                //remain idle
            }
            AnimationLoops--;
        }
        Arrival = null;
        if (CurrentWaypoint.AdvanceToNextWaypoint)
        {
            GoToNextPoint();
        } 
    }

    private bool MovementAnimationFinished()
    {
        return !(Anim.GetBool(MovementType.Run.ToString())
            || Anim.GetBool(MovementType.Walk.ToString())
            || Anim.GetBool(MovementType.Sprint.ToString()));
    }

    private bool IsIdle()
    {
        bool idle = Anim.GetCurrentAnimatorStateInfo(0).IsName("Idle");
        return idle;
    }

    private void ForceStopArrivalCoroutine()
    {
        AnimationLoops = 0;
        if (Arrival != null)
        {
            StopCoroutine(Arrival);
        }
    }

    private void EnableSingleAnimation(MovementType animation)
    {
        if (animation != MovementType.None)
        {
            Anim.SetBool(animation.ToString(), true);
        }
        if (MovementType.Run != animation)
        {
            Anim.SetBool(MovementType.Run.ToString(), false);
        }
        if (MovementType.Walk != animation)
        {
            Anim.SetBool(MovementType.Walk.ToString(), false);
        }
        if (MovementType.Sprint != animation)
        {
            Anim.SetBool(MovementType.Sprint.ToString(), false);
        }
    }



}
