﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Navigation
{
    [CustomEditor(typeof(Path))]
    public class PathEditor : Editor
    {

        

        public override void OnInspectorGUI()
        {
            Path path = (Path)target;

            DrawDefaultInspector();

            if (GUILayout.Button("Add Waypoint"))
            {
                path.AddWaypoint();
            }
        }

    }
}
