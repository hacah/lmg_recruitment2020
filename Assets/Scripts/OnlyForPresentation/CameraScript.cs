﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    [SerializeField]private Cinemachine.CinemachineVirtualCamera Camera;
    private NPCController controller;

    void Start()
    {
        StartCoroutine(FindNPC());
    }

    private void Update()
    {
        if (controller != null)
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                controller.GoToNextPoint();
            }
        }
    }

    private IEnumerator FindNPC()
    {
        yield return new WaitForSecondsRealtime(1f);
        controller = GameObject.FindObjectOfType<NPCController>();
        if (controller == null)
        {
            StartCoroutine(FindNPC());
        }
        else
        {
            GameObject NPC = controller.gameObject;
            //Camera.Follow = NPC.transform;
            Camera.LookAt = NPC.transform;
        }
    }
}
