﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Navigation;

public class NPCSpawner : MonoBehaviour
{
    [SerializeField] private GameObject NPC;
    [SerializeField] private Waypoint SpawnLocation;
    [SerializeField] private bool SpawnOnStart;
    [SerializeField][Tooltip("How many NPSs can this spawner spawn? Set to 0 for infinity.")]
    private int SpawnLimit = 1;
    public float SpawnDelay;
    private bool infiniteSpawner = false;

    private void Awake()
    {
        if (SpawnLimit == 0)
        {
            infiniteSpawner = true;
        }
        NPCController controller = NPC.GetComponent<NPCController>();
        if (controller == null)
        {
            Debug.LogError("NPC Prefab does not have an NPC Controller, the spawner will be disabled.", this);
            SpawnLimit = 0;
            infiniteSpawner = false;
        }
    }

    private void Start()
    {
        if (SpawnOnStart)
        {
            StartCoroutine(DelaySpawn(SpawnDelay));
        }
    }

    public void Spawn()
    {
        if (infiniteSpawner || SpawnLimit > 0)
        {
            SpawnLimit--;
            GameObject npc = Instantiate(NPC, SpawnLocation.transform.position, SpawnLocation.transform.rotation);
            NPCController controller = npc.GetComponent<NPCController>();
            controller.SetStartingPoint(SpawnLocation);
        }
    }

    private IEnumerator DelaySpawn(float delay)
    {
        yield return new WaitForSeconds(delay);
        Spawn();
    }
}
